Post.create([
  # Headlines
  {key: 'header_service', icelandic: "ÞJÓNUSTA", english: 'Products and services'},
  {key: 'header_about', icelandic: "UM ATTENTUS", english: 'About Attentus'},
  {key: 'header_staff', icelandic: "STARFSMENN", english: 'Our team'},
  {key: 'header_contact', icelandic: "HAFA SAMBAND", english: 'Contact us'},

  # Four column segment
  {key: 'service_title_1', icelandic: "MANNAUÐSSTJÓRI\nTIL LEIGU", english: 'OUTSOURCED HR SERVICES'},
  {key: 'service_title_2', icelandic: "RÁÐGJÖF\n& ÞJÓNUSTA", english: "CONSULTANCY\nSERVICES"},
  {key: 'service_title_3', icelandic: "FRÆÐSLA\n& ÞJÁLFUN", english: "TRAINING AND\nDEVELOPMENT"},
  {key: 'service_title_4', icelandic: "ÚTTEKTIR\n& GREININGAR", english: "AUDITS AND\n ANALYSIS"},

  {key: 'service_text_1', 
    icelandic: "Attentus tekur að sér að sinna mannauðsstjórnun í fyrirtækjum og stofnunum. Við bjóðum hagkvæma, einfalda og áhrifaríka leið til að nýta tíma stjórnenda betur og auka rekstrar-árangur með markvissri mannauðsstjórnun.", 
    english: 'Attentus provides effective and flexible HR services of exceptional quality, either for permanent or interim assignments. A simple and effective way to increase operations efficiency by strategically managing your HR processes – and make better use of your own time.'},
  {key: 'service_text_2', 
    icelandic: "Ráðgjöf okkar og þjónusta byggir á fagþekkingu og áratuga reynslu á öllum sviðum mannauðsmála, innri samskiptum og fræðslumálum, á sviði vinnuréttar og kjaramála og á sviði stjórnunar og stefnumótunar.", 
    english: 'Our consultancy services are based on professional knowledge and decades of experience in several fields of HR management, internal communication, training and development, labor law and company law, strategy planning etc.'},
  {key: 'service_text_3', 
    icelandic: "Viltu vita hver staðan er í raun og veru? Við önnumst ýmiss konar greiningar s.s. áreiðanleikakannanir, stjórnsýsluúttektir,viðhorfs-kannanir, jafnlaunagrein-ingar, stjórnendamat og ýmiss konar stöðumat sem tengist árangri á sviði mannauðsmála.", 
    english: 'We perform audits and analysis, such as diligence, administrative assessment, equal salary analysis, attitude surveys and management assessment. We have specialized in status analysis and assessment in various fields of HR, management and communication.'},
  {key: 'service_text_4', 
    icelandic: "Hæfni starfsmanna er lykillinn að árangri. Við bjóðum áhrifaríkar leiðir við greiningu hæfni,fræðsluþarfa og ið gerð þjálfunaráætlana. Við önnumst einnig fræðslu og þjálfun fyrir stjórnendur og starfsfólk.", 
    english: 'The competencies of human resources are an important key to organizational success. We offer effective solutions on analyzing competencies, training needs and conduct training and development for managers and other employees.'},

  # About Attentus
  {key: 'about_text', icelandic: "Attentus veitir þjónustu og ráðgjöf um allt sem snýr að rekstri út frá áherslum mannauðsstjórnunar.\n\nÞjónustan byggir á fagþekkingu, reynslu og metnaði með það að markmiði að auka rekstrarárangur.\n\nVið leggjum áherslu á einfaldar og skilvirkar leiðir sem stuðla að arðsemi rekstrar og ánægju starfsmanna og annarra hagsmunaaðila.\n\nRáðgjafar okkar hafa allir unnið við stjórnun í íslenskum fyrirtækjum og hafa víðtæka þekkingu og reynslu á sviði mannauðsstjórnunar.\n\nÁhersla er lögð á náið samstarf við stjórnendur og góður stuðningur við innleiðingu og eftirfylgni tryggir góðan árangur.\n\nAttentus – mannauður og ráðgjöf ehf. var stofnað árið 2007 af Árnýju Elíasdóttur, Ingu Björgu Hjaltadóttur og Ingunni Björk Vilhjálmsdóttur. Í dag eru auk stofnenda þær Guðríður Sigurðardóttir og Sigrún Þorleifsdóttir eigendur að fyrirtækinu og starfa þær allar sem ráðgjafar hjá Attentus.", 
   english: 'Attentus Consulting provides business consulting and services focusing on Human Resources. 
   
   Our services are based on professional knowledge and experience, strategically integrating HR processes and practices into daily operations, and increasing the efficiency and profitability of business operations.  
   
   All of our consultants have substantial management experience and a wide range of knowledge in the field of HR and business operations.
   
   We work closely with management on implementing our solutions, providing good support throughout the work process. 
   
   Attentus Consulting was founded in 2007 by Árný Elíasdóttir, Inga Björg Hjaltadóttir and Ingunn Björk Vilhjálmsdóttir. Today there are five partners, all working full time within the firm.'},
])
