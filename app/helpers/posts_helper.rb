module PostsHelper
  def xeditable?
    true # Or something like current_user.xeditable?
  end

  def format_ice(text)
    text.icelandic.gsub(/\r\n|\r|\n/, '<br/>').html_safe
  end

  def format_en(text)
    text.english.gsub(/\r\n|\r|\n/, '<br/>').html_safe
  end
end
