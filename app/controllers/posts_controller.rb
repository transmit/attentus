class PostsController < ApplicationController
  def index
    all_posts
    @feed_host = "http://blog.attentus.is"
    begin
    @feed = Feedzirra::Feed.fetch_and_parse("#{@feed_host}/?feed=rss2").entries.first
    rescue
    end
  end

  def english
    all_posts
  end

  def update
    post = Post.find(params[:id])
    post_params = params[:post]
    post.icelandic = post_params[:icelandic].gsub(/<br\/>/, "\n") if post_params[:icelandic]
    post.english = post_params[:english].gsub(/<br\/>/, "\n") if post_params[:english]
    post.save
    render json: post
    #render nothing: true
  end
  private

  def all_posts
    @editable = {}
    Post.all.each do |p|
      @editable[p.key.to_sym] = p
    end
  end

end
