class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :icelandic
      t.text :english
      t.string :key

      t.timestamps
    end
  end
end
