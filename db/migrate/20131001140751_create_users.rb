class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login, :null => false
      t.string :crypted_password, :null => false
      t.string :persistence_token, :null => false
      t.datetime "last_request_at"

      t.timestamps
    end
  end
end
